# Otomoto Car's Headings Scraper

Scraper downloads and saves in `.csv`, `.json` or `.yaml` file information found on otomoto website.
Scraper uses Firefox(geckodrive) as its selenium web driver. 

* `scraper.rb` 			- final version
* `archived-prototype/otomoto_scraper.rb` - prototype version, not for use
* `archived-prototype/part_screenshot.rb` - failed attempt at making small library to screenshot selected elements of HTML document.

### Usage
```ruby
require 'scraper.rb'

# default values and every option
scrap = Scraper.new car: 'toyota', pages: 10, headless: true
# starting scrapping, by default .csv is saved to .
scrap.start file_name: 'toyota', file_type: 'csv'

# by default file name will be called the same as \
# car name given in initializer
```

### Default values
#### Scraper#new
- `car: "toyota"`: name of the car available in otomoto  
- `pages: 10`: amount of pages that script will check
- `headless: true`: user can change this option to see what scripts is doing live on browser
#### Scraper#start
- `file_name: "#{@car}"`: name of target file, `@car` being name of the searched car
- `file_tpye: "csv"`: name of target file type, available {csv, json, yaml}

### Requirements
* `ruby        >=  2.5`
* `watir       :   7-beta` 
* `selenium    :   4-beta`
* `geckodriver :   0.29.1`

### Additional Notes
- [watir-screenshot-stitch](https://github.com/samnissen/watir-screenshot-stitch) - this library can be in future used to add full screen screenshot capability. Library tries to compensate selenium lack of full canvas screenshot and allow on Firefox only and with ImageMagick stitches multiply canvases.
- [watir-extensions-element-screenshot](https://github.com/ansoni/watir-extensions-element-screenshot) old extension to watir, unfortunatly not working anymore. Following this example element screenshot can be added if such functionality is required.
- [screenshot](https://github.com/amire80/screenshot/blob/master/lib/screenshot.rb) ruby extension to selenium, possible use in watir?