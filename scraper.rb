# frozen_string_literal: true

# ruby        >=  2.5
# watir       :   7-beta
# selenium    :   4-beta
# geckodriver :   newest

require 'watir'
require 'csv'
require 'json'
require 'yaml'

require_relative 'archived-prototype/part_screenshot'

# Watir Otomoto Scraper
class Scraper
  include PartScreenshot
  attr_accessor :element, :pages, :headless, :browser, :car_array

  def initialize(car: 'toyota', pages: 10, headless: true)
    puts 'initializing instance and browser'
    @car = car
    @pages = pages
    @headless = headless
    @car_array = []

    @browser = initialize_browser(headless)
    @browser.window.resize_to(1900, 1080)
  end

  def start(file_name: "#{@car}", file_type: 'csv')
    puts 'working on:'
    # fast_goto @car
    goto_otomoto
    accept_cookies
    search_and_find @car

    scrap

    puts 'finalizing:'
    save_to_file(file_name, file_type)
    clean_up
  end


  private

  def initialize_browser(headless)
    puts "\t1. initializing browser"

    Watir::Browser.new :firefox, headless: headless
  end

  # beta feature
  def fast_goto(car)
    puts "\t2,4. running to otomoto"

    @browser.goto("https://www.otomoto.pl/osobowe/#{car}/")
    sleep(1)
  end

  def goto_otomoto
    puts "\t2. going to otomoto"

    @browser.goto 'https://www.otomoto.pl/'
    sleep(1)
  end

  def accept_cookies
    puts "\t3. accepting cookie/s"

    @browser.button(id: 'onetrust-accept-btn-handler').click
    sleep(1)
  end

  def search_and_find(car)
    puts "\t4. searching for cars"

    # searching car
    @browser.text_field(class: 'ds-select').set car

    sleep(1)
    # enter to make above field select Toyota
    @browser.send_keys :enter
    # wait to make sure that server knows what we choose
    sleep(1)
    # and enter to search for those cars
    @browser.send_keys :enter

    # wait ot load page
    sleep(2)
  end

  def scrap
    puts "\t5. scraping"

    # index is used as id for saved car listings in their respective Hash
    while @pages.positive?
      cars = browser.articles(class: 'offer-item', data_test: 'search-result-item')

      cars.each_with_index do |car, index|

        temp_hash = { id: index, title: car.a(class: 'offer-title__link').title }

        # taking ul element that contains [price year mileage fuel_type]
        ul = car.ul(class: 'ds-params-block')

        # checking if this ul element is correct, otherwise it is advertisement element
        if ul.count == 4
          # saving price
          temp_hash[:price] = car.span(class: 'offer-price__number').text
          # saving year
          temp_hash[:year] = ul.li(data_code: 'year').span.text
          # saving mileage
          temp_hash[:mileage] = ul.li(data_code: 'mileage').span.text
          # saving engine capacity
          temp_hash[:engine_capacity] = ul.li(data_code: 'engine_capacity').span.text
          # saving type of fuel engine works on, or hybrid
          temp_hash[:fuel_type] = ul.li(data_code: 'fuel_type').span.text
        end
        # dumping to array
        @car_array << temp_hash
      end

      # next page
      # checks if next exist, if not means we have no more pages to work on so we break
      if browser.li(class: 'next').exists?
        browser.li(class: 'next').click
        @pages -= 1
      else
        p 'Left prematurely, not enough pages to scrap.'
        break
      end
      sleep(2)
    end

  end # end_of_scrapping

  def save_to_json(file_name)
    File.open(file_name, 'w') do |f|
      f.puts(JSON.pretty_generate(@car_array))
    end
  end

  def save_to_yaml(file_name)
    File.open(file_name, 'w') do |f|
      f.puts(@car_array.to_yaml)
    end
  end

  def save_to_csv(file_name)
    CSV.open(file_name, 'w') do |csv|
      csv << @car_array[0].keys

      @car_array.each do |car|
        csv << car.values
      end
    end
  end

  def save_to_file(file_name, file_type)
    puts "\t6. saving to file"

    case file_type
    when 'csv'
      save_to_csv("#{file_name}.csv")
    when 'json'
      save_to_json("#{file_name}.json")
    when 'yaml', 'yml'
      save_to_yaml("#{file_name}.#{file_type}")
    else
      puts "Wrong file_typ: #{file_type} argument was given."
      puts 'It should be either csv, json or yaml'
      puts "File will be saved as #{file_name}.csv"
      save_to_csv("#{file_name}.csv")
    end

  end # end_of_saving to file

  def clean_up
    puts "\t7. cleaning"
    @browser.close
  end

end # end_of_class
